﻿using ParcialDosCamila.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ParcialDosCamila
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BuscarPersonas : ContentPage
	{
		public BuscarPersonas ()
		{
			InitializeComponent ();
		}

        public void buscar()
        {
            if (string.IsNullOrEmpty(EntryCedula.Text))
            {
                DisplayAlert("Error", "Debe Digitar Cedula", "Ok");
            }
            else
            {
                int cedula = int.Parse(EntryCedula.Text);
                using (SQLite.SQLiteConnection conecction = new SQLite.SQLiteConnection(App.urlBd))
                {
                    var listaPersonas = conecction.Table<Personas>();
                    string x = null;
                    foreach (var aux in listaPersonas)
                    {
                        if (aux.Cedula == cedula)
                        {
                            Nombres.Text = aux.Nombres.ToString();
                            Apellidos.Text = aux.Apellidos.ToString();
                            Fecha.Text = aux.Fecha_Nacimiento.ToString();
                            
                            x = "Correcto";
                        }
                    }
                    if (x == null)
                    {
                        DisplayAlert("Advertencia", "No fue encontrado", "ok");
                    }
                }
            }
        }
        public void eliminar()
        {
            
                int cedula = int.Parse(EntryCedula.Text);
                using (SQLite.SQLiteConnection conecction = new SQLite.SQLiteConnection(App.urlBd))
                {
                    var listaPersonas = conecction.Table<Personas>().FirstOrDefault(delete => delete.Cedula == cedula);
                    string x = null;

                    if (listaPersonas != null)
                    {
                    conecction.Delete(listaPersonas);
                    
                    Nombres.Text = "";
                    Apellidos.Text = "";
                    Fecha.Text = "";
                    x = "Eliminado Con Exito";
                    DisplayAlert("Correcto", "Datos Eliminado", "ok");
                }

                    if (x == null)
                    {
                        DisplayAlert("Advertencia", "No Existe en la Base de Datos", "ok");
                    }
                }
            }
        }
    }