﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParcialDosCamila.Modelos
{
    class Personas
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }
        [MaxLength(150)]

        public int Cedula { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public DateTime Fecha_Nacimiento { get; set; }
    }
}
