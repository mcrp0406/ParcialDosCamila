﻿using ParcialDosCamila.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ParcialDosCamila
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ListarPersonas : ContentPage
	{
		public ListarPersonas ()
		{
			InitializeComponent ();
		}
        async private void mostrar(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MainPage());
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();

            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                List<Personas> listaPersonas;
                listaPersonas = connection.Table<Personas>().ToList();

                lista.ItemsSource = listaPersonas;
            }
        }

    }
}