﻿using ParcialDosCamila.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ParcialDosCamila
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        async private void buscar(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new BuscarPersonas());
        }

        async private void validar(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(entryNombres.Text) || string.IsNullOrEmpty(entryApellidos.Text) ||
                string.IsNullOrEmpty(entryCedula.Text))
            {
                await DisplayAlert("Error", "Debe Completar los Campos.", "Ok");
            }
            else
            {
                Validar1();

                entryApellidos.Text = "";
                entryNombres.Text = "";
                entryCedula.Text = "";
                await Navigation.PushAsync(new ListarPersonas());
            }
        }

        public void Validar1()
        {
            Personas persona = new Personas()
            {
                Cedula = int.Parse(entryCedula.Text),
                Nombres = entryNombres.Text,
                Apellidos = entryApellidos.Text,
                Fecha_Nacimiento = Fecha.Date
            };

            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                connection.CreateTable<Personas>();
                var result = connection.Insert(persona);

                if (result > 0)
                {
                    DisplayAlert("Correcto", "Se Creo Correctamente", "Ok");
                }
                else
                {
                    DisplayAlert("Error", "No Fue creada", "Ok");
                }
            };
        }
    }
}